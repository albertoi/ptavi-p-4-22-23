#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente UDP que abre un socket a un servidor
"""

import socket
import sys
# Constantes. Dirección IP del servidor y contenido a enviar
SERVER = "localhost"
PORT = int(sys.argv[2])
SALUDO = sys.argv[3:]
LETRA = " ".join(SALUDO)
IP = sys.argv[1]


def main():
    # Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            # print(f"Enviando a {SERVER}:{PORT}:", SALUDO)
            my_socket.sendto(LETRA.encode('utf-8'), (SERVER, PORT))
            my_socket.sendto(LETRA.encode('utf-8'), (SERVER, PORT))
            data = my_socket.recv(1024)
            print('Recibido: ', data.decode('utf-8'))   # lo que recibo del servidor lo imprimo
            print('Recibido: ', data.decode('utf-8'))   # Recibido otra vez
        print("Cliente terminado.")
    except ConnectionRefusedError:
        print("Error conectando a servidor")


if __name__ == "__main__":
    main()
