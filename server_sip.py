#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys
from time import gmtime, strftime, time


# Constantes. Puerto.
PORT = int(sys.argv[1])


class SIPRequest():

    def __init__(self, data):
        self.data = data

    def _get_address(self, uri):
        address = uri.split(':')[1]
        schema = uri.split(':')[0]
        return address, schema

    def _parse_command(self, line):
        metodo = line.split(" ")
        self.command = metodo[0]
        self.uri = metodo[1]
        self.address, schema = self._get_address(self.uri)
        if self.command == 'REGISTER':
            if schema == 'sip':
                self.result = '200 OK'
            else:
                self.result = '416 Unsupported URI Scheme'

        else:
            self.result = '405 Method Not Allowed'

    def parse(self):
        self._parse_command(self.data.decode('utf-8'))
    def _parse_headers(self, first_nl):
        pass


class SIPRegisterHandler(socketserver.BaseRequestHandler):
    Dic = {}

    def handle(self):
        data = self.request[0]
        sock = self.request[1]
        sip_request = SIPRequest(data)
        sip_request.parse()
        if (sip_request.command == "REGISTER") and (sip_request.result == "200 OK"):
            self.process_register(sip_request)
        sock.sendto(f"SIP/2.0 {sip_request.result}\r\n\r\n".encode(), self.client_address)
        print("Lo que tengo es: " + str(self.Dic))

    def process_register(self, elemento):
        self.Dic[self.client_address[0]] = elemento.address

def main():
    # Listens at port PORT (my address)
    # and calls the EchoHandler class to manage the request
    try:
        serv = socketserver.UDPServer(('', PORT), SIPRegisterHandler)
        print(f"Server lisening in port {PORT}...")
    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
        sys.exit(0)


if __name__ == "__main__":
    main()
