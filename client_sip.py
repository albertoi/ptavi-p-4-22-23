#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente UDP que abre un socket a un servidor
"""

import socket
import sys
# Constantes. Dirección IP del servidor y contenido a enviar


def main():
    SERVER = sys.argv[1]
    PORT = int(sys.argv[2])
    REGISTER = sys.argv[3]
    ADD = sys.argv[4]

    # Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            SOLICITUD = REGISTER.upper() + " " + "sip: " + ADD + " " + "SIP/2.0\r\n\r\n"
            my_socket.sendto(SOLICITUD.encode('utf-8'), (SERVER, PORT))
            # print(f"Enviando a {SERVER}:{PORT}:", SALUDO)
            data = my_socket.recv(1024)
            print('Recibido: ', data.decode('utf-8'))   # lo que recibo del servidor lo imprimo
        print("Cliente terminado.")
    except ConnectionRefusedError:
        print("Error conectando a servidor")


if __name__ == "__main__":
    main()
